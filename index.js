var fs = require('fs');
const chaptersLocation = process.argv[2];
var fileOperation = require('./utils/fileOperation');
const excelGen = require('./excel-generator');
const url = "http://lmhosting.learningmate.com/jwy";
const project = "Project:Wiley project JWY06";
const projectCode = "JWY06";
const title = "Interactive tutorial Selection/LO section";
const version = chaptersLocation.split("\\")[chaptersLocation.split("\\").length - 1];

let infoArry = [];

if( chaptersLocation ) {
    if ( fs.existsSync( chaptersLocation ) ){
        fs.readdir( chaptersLocation, (err, dirContent) => {
            if( err ) {
                console.log('Getting err while reading folder -->', chaptersLocation);
            } else {
                let chapterLocArr = projectValidator( chaptersLocation, dirContent);
                if( chapterLocArr.length != 0 ) {
                    let finalInfoArry = [];
                    chapterLocArr.forEach( (ch, idx) => {
                        fs.readdir( ch, ( err, chContent) => {
                            if( err ) {
                                console.log('Getting err while reading folder -->', ch);
                            } else {
                                gatherProjectInfo(ch, chContent, idx).then( res1 => {
                                    finalInfoArry.push(res1);
                                    //console.log(res1);
                                    if( idx ===  chapterLocArr.length - 1) {
                                        excelGen({
                                            data: finalInfoArry,
                                            url: url,
                                            title: title,
                                            project: project,
                                            version: version,
                                            projectCode: projectCode,
                                            chaptersLocation: chaptersLocation
                                        }).then( res3 => {
                                            console.log(res3);
                                        }).catch( err3 => {
                                            console.log('Error!', err);
                                        });
                                    }
                                }).catch( err1 => {
                                    console.log('Error!', err1);
                                })
                            }
                        })
                    });
                } else {
                    console.log('Invalid location');
                }
            }
        });
    } else {

    }
} else {
    console.log('Please input a chapter location!.');
}

function projectValidator( location, dirArr ) {
    let chaptersArr = [];
    dirArr.forEach( dir => {
        let chapterLoc = location + '\\' + dir; 
        if( /CH[0-9][0-9]/.test( dir ) && fs.lstatSync( chapterLoc ).isDirectory() ) {
            chaptersArr.push( chapterLoc );
        }
    });

    return chaptersArr;
}

function gatherProjectInfo( loc, contentArr, index) {
    //console.log(contentArr);
    let array = [];
    return new Promise( ( resolve, reject ) => {
        let i = 0;
        contentArr.forEach( (ch,idx) => {
            
            if ( /LO[0-9][0-9]_[0-9]/.test( ch ) && fs.lstatSync( loc + '\\' +ch  ).isDirectory() ) {
                let data = JSON.parse(fs.readFileSync(loc + '\\' +ch + '\\data\\lodata.json'));
                array.push({
                    title: data.title,
                    loc: loc + '\\' +ch + '\\data\\lodata.json',
                    section: (index+1).toString() + '.' + ( i + 1).toString(),
                    chapterId: (index+1),
                    chapter: loc.split("\\")[loc.split("\\").length - 1]
                });
                i++;
            }
        })
        resolve(array);
    })
}
