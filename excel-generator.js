var Excel = require('exceljs');
var workbook = new Excel.Workbook();

const sheetTitleRowNo = 1;
const projectTitleRowNo = 3;
var columnStartRowNo = 5;
const date = new Date().getTime()

module.exports = function(info) {
    return new Promise(
        ( resolve, reject ) => {


            var worksheet = workbook.addWorksheet('Chapters');

            worksheet.getRow(sheetTitleRowNo).values = [info.title]; worksheet.getRow(1).font = {bold: true};
            worksheet.getRow(projectTitleRowNo).values = [info.project]; worksheet.getRow(3).font = {bold: true};

            /*Column headers*/
            worksheet.getRow(columnStartRowNo).values = ['Chapter#', 'Chapter title', 'Section/LO', 'Direction Section/LO link path'];
            worksheet.getRow(columnStartRowNo).font = { bold: true };
            worksheet.columns = [
                { key: 'ch', width: 10 },
                { key: 'chTitle', width: 20 },
                { key: 'loTitle', width: 62 },
                { key: 'link', width: 72 }
            ]

            let sheetData = info.data;
            //console.log(sheetData)
            sheetData.forEach( (chapter, chId) => {
                chapter.forEach( (lo, loId) => {
                    if( loId === 0) {
                        worksheet.addRow(
                            {
                                ch: loId === 0 ? lo.chapterId: '',
                                chTitle: loId === 0 ? lo.chapter: '',
                                loTitle: '',
                                link: ''
                            }
                        )
                    }
                    worksheet.addRow(
                        {
                            ch: '',
                            chTitle: '',
                            loTitle: lo.section + ' ' + lo.title,
                            link: {
                                text: info.url + '/' + info.projectCode + '/' + info.version + '/' +'/'+ lo.chapter + '/' + (lo.chapterId < 10? 'CH0' + lo.chapterId: 'CH' + lo.chapterId) + '_Complete/index.html?lo='+(loId+1),
                                hyperlink: info.url + '/' + info.projectCode + '/' + info.version +'/'+ lo.chapter + '/' + (lo.chapterId < 10? 'CH0' + lo.chapterId: 'CH' + lo.chapterId) + '_Complete/index.html?lo='+(loId+1)
                            }
                        }
                    )
                    //columnStartRowNo++;
                });
                //worksheet.getRow(columnStartRowNo).values = [];
            });


            workbook.xlsx.writeFile( info.chaptersLocation + '\\' + 'lo_level_links_sheet'+ date +'.xlsx').then(function() {
                resolve('Excel file created! --> ' + info.chaptersLocation + '\\' + 'lo_level_links_sheet'+ date +'.xlsx');
            });
        }
    );
}
