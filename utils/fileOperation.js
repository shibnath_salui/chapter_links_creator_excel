var fs = require('fs');

module.exports = {
    
    createFolder: ( path ) =>{
        return new Promise( (resolve, reject) =>{
            fs.mkdir(path, function (err) {
                if (err) {
                    console.log('failed to create directory', err);
                    reject(false);
                } else {
                    resolve(true);
                }
            });
        })
    },
    
    readFileContent: ( path ) =>{
        return new Promise( (resolve, reject) =>{
            fs.readFile( path, function (err, data) {
                if (err) {
                    console.log('Fails to read file: ' + path + '/', err);
                    reject({
                        success: false,
                        err: err
                    });
                } else {
                    resolve({
                        success: true,
                        data: JSON.parse(data)
                    });
                }
            });
        })
    },
    
    writeFile: ( path, data ) =>{
        return new Promise( (resolve, reject) =>{
            fs.writeFile( path, data, function (err) {
                if (err) {
                    console.log('Fails to write file: ' + path + '-------', err);
                    reject(false);
                } else {
                    resolve(true);
                }
            });
        })
    },
    
    copyFile: (source, target) =>{
        var rd = fs.createReadStream(source);
        var wr = fs.createWriteStream(target);
        return new Promise(function(resolve, reject) {
          rd.on('error', reject);
          wr.on('error', reject);
          wr.on('finish', resolve);
          rd.pipe(wr);
        }).catch(function(error) {
          rd.destroy();
          wr.end();
          throw error;
        });
    }
}