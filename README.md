# Excel file creator

### Installation & user manual:
To intsall and run this application, you must need to have lastest nodeJS and npm installed on your system.
Once you install node on your system, please follow the step below:

- Open a CMD and navigate to directory where you wish to install this application.
- Clone the repo(https://shibnath_salui@bitbucket.org/shibnath_salui/chapter_links_creator_excel.git) by using the follwoing command below:
   > git clone https://shibnath_salui@bitbucket.org/shibnath_salui/chapter_links_creator_excel.git
- Then navigate to that cloned repo by using following command:
   > cd  chapter_links_creator_excel
- Intsall the dependecies by using the following command below:
   > npm i
- As soon as depepecies get installed and now you need to run application by using 
- Now the application is ready for use
- Copy the window path location where your Wiley project chapter is residing.
- Pleas remember this copied path we are going to use as the command line input to the application.
- Now use the following command after navigating to the project folder:
    > node index.js <paste the folder location where your Wiley chapters are residing>

### Bibliography
- https://nodejs.org/en/
- https://www.npmjs.com/package/exceljs
- https://stackoverflow.com/questions/17450412/how-to-create-an-excel-file-with-nodejs

#### Thank you!